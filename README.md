package FactoryPattern;

public class FactoryPattern {
	
	//Patrón de creación de objetos
	//Objects creation pattern

	public static void main(String[] args) {
	 	ShapeFactory shapeFactory = new ShapeFactory();
	 	
	 	Shape shape1 = shapeFactory.getShape("Circle");
	 	shape1.draw();
	 	Shape shape2 = shapeFactory.getShape("Rectangle");
	 	shape2.draw();
	 	
	 	Shape shape3 = shapeFactory.getShape("Square");
	 	shape3.draw();
	 	

	}

}



---

package FactoryPattern;

public class ShapeFactory {
	
	public Shape getShape(String shapeType) {
		if (shapeType == null) {
			return null;
		}
		if (shapeType.equalsIgnoreCase("Circle")) {
			return new Circle();
		}
		
		if (shapeType.equalsIgnoreCase("Rectangle")) {
			return new Rectangle();
		}
		
		if (shapeType.equalsIgnoreCase("Square")) {
			return new Square();
		}
		
		return null;
	}
}


---

package FactoryPattern;

public interface Shape {
	void draw();
}

---

package FactoryPattern;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method.");
		
	}
	
}

---

package FactoryPattern;

public class Rectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
		
	}

}

---

package FactoryPattern;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Square::draw() method.");
		
	}

}